import React, { useState, useEffect } from 'react';
import { getStorage } from '../utils/storage';
import { Link } from 'react-router-dom';

function ProfilePage() {
    const [ name, setName ] = useState('');

    useEffect(() => {
        setName(getStorage('user'));
    }, []);

    const onLogoutClicked = () => localStorage.clear();
    const onClearClicked = () =>  {
        localStorage.removeItem('words')
    }

    let words = getStorage('words').map((word, index) => { return (

        <div key={index}>{word}</div>

        )})

    return (
        <div className="container">
            <p>Welcome to your page {name}</p>
            {words}
            <Link to="/translator"> 
                <button className="btn" type="button">Go back to translating</button>
            </Link>
            <Link to='/home'>
            <button className="logout" type='button' onClick={onLogoutClicked}>Logout</button>
            </Link>
            <Link to='/translator'>
            <button className="btn" type="button" onClick={onClearClicked}>Clear history</button>
            </Link>
        </div>
    );
}

export default ProfilePage;