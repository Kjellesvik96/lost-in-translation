import React, { useState, useEffect } from 'react';
import { getStorage, setStorage } from '../utils/storage';
import {LETTERS} from '../utils/pictureLoader';
import { Link } from 'react-router-dom';

const Translation = () => {

    const [name, setName] = useState('');
    const [word, setWord] = useState('');
    const [translated, setTranslated] = useState([]);

    useEffect(() => {
        setName(getStorage('user'));
    }, []);

    const handleTextChange = (input) => setWord(input.target.value.trim().toLowerCase());

    const handleOnClick = () => {
        document.getElementById("input").value = '';
        let translate = [];
        let chars = word.trim().split('');
        for (let i = 0; i < chars.length; i++) {
            if (!/[a-zA-Z]/g.test(chars[i])) {
                translate.push(chars[i]);
            } else {
                translate.push(LETTERS[chars[i]]);
            }
        }
        const currentWords = getStorage('words') || [];
        setStorage('words', [...currentWords, word]);
        setTranslated(translated);

        const pictures = translate.map((char, index) => <img src={char} alt="letters" key={index}/>);
        setTranslated(pictures);
    }

    return (
        <div className="container">
            <div className="headerBar">
                <p>Welcome {name}!</p>
                <Link to="/profile">
                    <button className="btn" disabled={localStorage.length === 1} type="button">Click here to go to your page</button>
                </Link>
            </div>
            <form className="form">
                <input id="input" className='input' type='text' placeholder='Text to translate' 
                        minLength="1" maxLength="40" onChange={handleTextChange}/>
                <button className='btn' type="button" onClick={handleOnClick}>Translate</button>
                <div className="output">
                </div>
                {translated}
            </form>
        </div>
    );
}

export default Translation;