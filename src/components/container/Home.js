import React, {useState, useEffect} from 'react';
import {getStorage, setStorage} from '../utils/storage';
import { Link } from 'react-router-dom';

const Home = () => {
    const [ isLoggedIn, setIsLoggedIn ] = useState(false);
    const [ name, setName ] = useState(''); 

    useEffect(() => {
        setIsLoggedIn(getStorage('user'));
    }, []);

    const onNameChange = (input) => setName(input.target.value.trim());
    const onLoginClicked = () => {
        setStorage('user', name);
        setIsLoggedIn(true);
    }

    return  (
        <div className="container">
            <h2>Enter your name to start translating</h2>
            <input className="input" type="text" placeholder="Enter your name..." onChange={onNameChange}/>
            <Link to='/translator'>
            <button type='button' className='btn' onClick={onLoginClicked}>Enter</button>
            </Link>
        </div>
    )
};

export default Home;