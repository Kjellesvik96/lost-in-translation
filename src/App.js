import React from 'react';
import './App.css';
import { BrowserRouter as Router, 
  Switch, 
  Route, 
  Redirect 
} from 'react-router-dom';
import Home from './components/container/Home';
import Translation from './components/container/Translation';
import ProfilePage from './components/container/ProfilePage';
import NotFound from './components/container/NotFound';

function App() {
  return (
    <Router>
      <div className='App'>
      <h1 className="header">Lost In Translation</h1>
      <Switch>
        <Route exact path='/'>
          <Redirect to='/home'/>
        </Route>
        <Route exact path='/home' component={Home}/>
        <Route path='/translator' component={Translation}/>
        <Route path='/profile' component={ProfilePage}/>
        <Route path='/*' component={NotFound}/>
      </Switch>
    </div>
    </Router>
  );
}

export default App;