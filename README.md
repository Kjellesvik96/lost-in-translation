REACT Assignment Lost in Translation

Due: 31.08.2020

Run app sends you to home page. Here you can login get redirected to translate page.
On the translate page you can translate words from normal text to sign language.
From the translate page you can head over to your profile to check your history of words translated.
On your profile page you can see your last translated words. You can sign out of the app and get redirected to home page.
You can also head back to the translation page to continue translating stuff.

Known bugs 28.08.20:
    - The text-input does not understand other signs/letters than "a-z/A-Z"
    - You need to delete text-input data before entering new word
    - If you access the profilepage before translating, will crash app
    - "Clear" button on profilepage will empty local storage, but also crash app 